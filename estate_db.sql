--
-- Database: `estate_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `kuca_tbl`
--

CREATE TABLE `kuca_tbl` (
  `kuca_id` int(11) NOT NULL,
  `ime` varchar(40) COLLATE utf8_croatian_ci NOT NULL,
  `pocetnaCena` int(11) NOT NULL,
  `slikaKuce` varchar(50) COLLATE utf8_croatian_ci NOT NULL,
  `opisKuce` varchar(100) COLLATE utf8_croatian_ci NOT NULL,
  `sifraKuce` varchar(40) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `kuca_tbl`
--

INSERT INTO `kuca_tbl` (`kuca_id`, `ime`, `pocetnaCena`, `slikaKuce`, `opisKuce`, `sifraKuce`) VALUES
(4, 'Kuca u Berlinu', 6000, 'slike\\slikeKuca\\kuca1.jpg', 'Prelepa kuca na livadi', 'kuca1'),
(5, 'Kuca u Cacku', 5000, 'slike\\slikeKuca\\kuca2.jpg', 'Prelepa porodicna kuca sa terasom', 'kuca2'),
(6, 'Plava vila', 6000, 'slike\\slikeKuca\\kuca3.jpg', 'Velika plava vila sa bazenom i jos mnogo toga', 'kuca3'),
(8, 'Pametna kucica', 15000, 'slike\\slikeKuca\\kuca5.jpg', 'Kuca dvadeset prvog veka.', 'kuca5'),
(9, 'Zlatiborac', 5000, 'slike\\slikeKuca\\kuca4.jpg', 'Planinska kuca sa pogledom na prirodu', 'kuca4');

-- --------------------------------------------------------

--
-- Table structure for table `kupac_tbl`
--

CREATE TABLE `kupac_tbl` (
  `kupac_id` int(11) NOT NULL,
  `ime` varchar(40) COLLATE utf8_croatian_ci NOT NULL,
  `sifra` varchar(40) COLLATE utf8_croatian_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `kupac_tbl`
--

INSERT INTO `kupac_tbl` (`kupac_id`, `ime`, `sifra`) VALUES
(4, 'Mitar', 'Veljic'),
(5, 'ana', 'anic'),
(6, 'Milica', 'vasic'),
(7, 'mirko', 'stanisic');

-- --------------------------------------------------------

--
-- Table structure for table `ponuda_tbl`
--

CREATE TABLE `ponuda_tbl` (
  `ponuda_id` int(11) NOT NULL,
  `kuca_fk` int(11) NOT NULL,
  `kupac_fk` int(11) NOT NULL,
  `iznos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_croatian_ci;

--
-- Dumping data for table `ponuda_tbl`
--

INSERT INTO `ponuda_tbl` (`ponuda_id`, `kuca_fk`, `kupac_fk`, `iznos`) VALUES
(15, 4, 4, 6050),
(16, 4, 4, 7000),
(17, 5, 5, 6000),
(18, 5, 5, 6000),
(19, 5, 5, 7000),
(20, 6, 5, 7000),
(21, 4, 5, 7100),
(22, 6, 6, 8000),
(23, 4, 7, 15000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kuca_tbl`
--
ALTER TABLE `kuca_tbl`
  ADD PRIMARY KEY (`kuca_id`),
  ADD UNIQUE KEY `sifraKuce` (`sifraKuce`);

--
-- Indexes for table `kupac_tbl`
--
ALTER TABLE `kupac_tbl`
  ADD PRIMARY KEY (`kupac_id`);

--
-- Indexes for table `ponuda_tbl`
--
ALTER TABLE `ponuda_tbl`
  ADD PRIMARY KEY (`ponuda_id`),
  ADD KEY `kuca_fk` (`kuca_fk`),
  ADD KEY `kupac_fk` (`kupac_fk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kuca_tbl`
--
ALTER TABLE `kuca_tbl`
  MODIFY `kuca_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kupac_tbl`
--
ALTER TABLE `kupac_tbl`
  MODIFY `kupac_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `ponuda_tbl`
--
ALTER TABLE `ponuda_tbl`
  MODIFY `ponuda_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ponuda_tbl`
--
ALTER TABLE `ponuda_tbl`
  ADD CONSTRAINT `ponuda_tbl_ibfk_1` FOREIGN KEY (`kuca_fk`) REFERENCES `kuca_tbl` (`kuca_id`),
  ADD CONSTRAINT `ponuda_tbl_ibfk_2` FOREIGN KEY (`kupac_fk`) REFERENCES `kupac_tbl` (`kupac_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
